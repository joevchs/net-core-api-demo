﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;

namespace Inclutec.Core.Security
{
    public class SecureAllEndpointsFilterPolicy : AuthorizeFilter
    {

        public SecureAllEndpointsFilterPolicy()
            : base(new AuthorizationPolicyBuilder()
                       .RequireAuthenticatedUser()
                       .Build())
        {
        }

    }
}