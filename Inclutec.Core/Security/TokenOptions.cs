﻿namespace Inclutec.Core.Security
{
    public class TokenOptions
    {
        public string Issuer { get; set; }
        public string ScopeName { get; set; }
        public string ScopeSecretKeyCypher { get; set; }
        public string IssuerUrl { get; set; }
    }
}
