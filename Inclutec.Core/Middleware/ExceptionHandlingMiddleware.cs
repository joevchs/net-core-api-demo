using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using Newtonsoft.Json;
using System.IO;
using MimeKit;
using Inclutec.Core.Email;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Routing;

public class ExceptionHandlingMiddleware
{
    #region Declaracion de variables
    private readonly RequestDelegate _next;
    private readonly ILogger<ExceptionHandlingMiddleware> _logger;
    private IEmailService _emailService;
    private ConfiguracionesGlobales _configGlobal;
    #endregion

    // Constructor, injectar servicios y configuracion
    public ExceptionHandlingMiddleware (
        RequestDelegate next, 
        ILoggerFactory loggerFactory, 
        IEmailService emailService,
        IOptions<ConfiguracionesGlobales> config)
    {
        _next = next ?? throw new ArgumentNullException(nameof(next));
        _logger = loggerFactory?.CreateLogger<ExceptionHandlingMiddleware>() ?? throw new ArgumentNullException(nameof(loggerFactory));
        this._emailService = emailService;
        _configGlobal = config.Value;
    }

    // Cuando sucede una excepcion se ejecuta este metodo
    public async Task Invoke(HttpContext context)
    {
        try
        {
            await _next.Invoke(context);
            if (context.Response.StatusCode == StatusCodes.Status404NotFound)
            {
                
            }
        }
        catch(Exception exception)
        {
            if (context.Response.HasStarted)
            {
                _logger.LogWarning("The response has already started, the http status code middleware will not be executed.");

                throw;
            }

            // Aqui podemos hacer lo que queramos con la excepcion
            // En este caso obtenemos todo el stack trace y los detalles
            var response = context.Response;
            var status = HttpStatusCode.InternalServerError;
            var errorMessage = exception.Message;
            var innerException = exception.InnerException;
            var source = exception.Source;
            var exceptionType = exception.GetType();
            var stack = exception.StackTrace;
            
            var routeData = context.GetRouteData() ?? new RouteData();
            var controller = routeData != null ? routeData.Values["controller"] : "";
            var actionName = routeData != null ? routeData.Values["action"] : "";

            var lineNumber = 0;
            var colNumber = 0;
            var fileName = "";
            var trace = new StackTrace(exception, true);
            var frame = trace.GetFrames().FirstOrDefault();

            if (frame != null)
            {
                lineNumber = frame.GetFileLineNumber();
                colNumber = frame.GetFileColumnNumber();
                fileName = frame.GetFileName();
            }

            // Request Connection IP
            var ipv4Address = string.Empty;
            var connection = context.Connection;
            var ip = connection?.RemoteIpAddress;
            if (ip != null)
            {
                var ipv4 = ip.MapToIPv4();
                ipv4Address = ipv4.ToString();
            }

            // Current user identity
            var principal = context.User;
            var identity = principal.Identity as ClaimsIdentity;
            var userId = "N/A";
            var fullname = "N/A";

            if (identity != null)
            {
                if (identity.IsAuthenticated)
                {
                    try
                    {
                        fullname = identity.FindFirst(ClaimTypes.Name)?.Value;
                        userId = identity.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                        // Aqui podemos obtener cualquier claim del usuario actual
                        // ...
                    }
                    catch (Exception e)
                    {
                        _logger.LogWarning(default(EventId), "User Full Name wasn't found in the Bearer Token Claims. Error: "+e.Message);
                    }
                }
            }

            var customMessage = "Error interno del servidor. Si este error persiste por favor contacte al administrador.";
            var isCritical = false;
            
            // Common exceptions, overrides the defaults with multilenguage
            if (exception is UnauthorizedAccessException)
            {
                customMessage = "Usted no esta autorizado para acceder a este recurso. Si este error persiste por favor contacte al administrador.";
                
                status = HttpStatusCode.Unauthorized;
            }
            else if (exception is NotImplementedException)
            {
                customMessage = "El recurso accedido esta en desarrollo. Si este error persiste por favor contacte al administrador.";
                status = HttpStatusCode.NotImplemented;
            }
            else if (exception is ArgumentNullException)
            {
                customMessage = "Ocurrió un error por valor vacío. Si este error persiste por favor contacte al administrador.";
            }
            else if (exception is DivideByZeroException)
            {
                customMessage = "Error division por 0. Si este error persiste por favor contacte al administrador.";
            }
            else if (exception is FileNotFoundException)
            {
                customMessage = "Error no se encontro el archivo. Si este error persiste por favor contacte al administrador.";
                status = HttpStatusCode.NotFound;
            } else
            {
                customMessage = errorMessage;
            }

            var logStackTrace = JsonConvert.SerializeObject(new
                                                               {
                                                                   Error = new
                                                                   {
                                                                       UserName = fullname,
                                                                       FileName = fileName,
                                                                       Controller = controller,
                                                                       ActionMethod = actionName,
                                                                       LineNumber = lineNumber,
                                                                       ColumnLineNnumber = colNumber,
                                                                       StatusCode = (int)status,
                                                                       CustomMessage = customMessage,
                                                                       ErrorMessage = errorMessage,
                                                                       StackTrace = stack,
                                                                       Source = source,
                                                                       InnerException = innerException,
                                                                       Exception = exceptionType.Name,
                                                                       IP = ipv4Address
                                                                   }
                                                               });

            // Retornar la respuesta del error al usuario
            var result = JsonConvert.SerializeObject(new { error = customMessage });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)status;
            await context.Response.WriteAsync(result);

            // Despues de retornar la respuesta al cliente
            // Hacer lo que sea con la excepcion aca abajo
            if (isCritical)
            {
                _logger.LogCritical(logStackTrace);
            }
            else
            {
                _logger.LogError(logStackTrace);
            }
            
            if (_configGlobal.SendNotificationEmailOnException) {
                var to = new List<MailboxAddress>
                {
                    new MailboxAddress("jostin95ev@hotmail.com"),
                    new MailboxAddress("evandbmw@gmail.com"),
                    new MailboxAddress("jochaves@itcr.ac.cr")
                };

                var fecha = DateTime.Now;
                _emailService.EnviarCorreo("<p><b>[TALLER]</b> El sistema notifica que ocurrió una excepción.</p><p>Por favor revisar los logs en la fecha " + fecha + " lo más pronto posible.</p>", "ALERTA IMPORTANTE", to);
            }

            return;
        }
    }
}