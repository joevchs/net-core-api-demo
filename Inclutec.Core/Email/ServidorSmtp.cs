﻿/*

Clase para guardar los posibles servidores y puertos desde donde se puede enviar correo electronico

*/ 
namespace Inclutec.Core.Email
{
    public class ServidorSmtp
    {
        public string Nombre { get; set; }
        public string Servidor { get; set; }
        public string Dominio { get; set; } = "smtp.live.com";
        public int Puerto { get; set; } = 587;
        public bool UsarSsl { get; set; } = false;
    }
}
