﻿/*
 
Clase para guardar los posibles servidores smtp

*/

using System.Collections.Generic;

namespace Inclutec.Core.Email
{
    public class AppConfig
    {
        public List<ServidorSmtp> ServidoresSmtp { get; set; }
    }
}
