﻿/*
Clase para guardar los valores de configuracion globales de la aplicacion
Se inicializan con valores por defecto en caso de no definirse en el appsettings.json
*/ 
namespace Inclutec.Core.Email
{
    public class ConfiguracionesGlobales
    {
        public string AppName { get; set; } = "Defina el nombre de la aplicacion";
        public string CorreoPrincipal { get; set; } = "";
        public string NombrePrincipal { get; set; } = "App";
        public string ContraseniaCorreo { get; set; } = "";
        public string AsuntoDefaultCorreoError { get; set; } = "Sin asunto";
        public bool SendNotificationEmailOnException { get; set; } = false;
        public string Secret { get; set; }
    }
}
