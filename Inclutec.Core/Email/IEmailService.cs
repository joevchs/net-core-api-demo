﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MimeKit;

namespace Inclutec.Core.Email
{
    public interface IEmailService
    {
        Task EnviarCorreo(string mensajeHtml, string asunto, List<MailboxAddress> to);
        Task EnviarCorreo(string mensajeHtml, string asunto, string from, string password, List<MailboxAddress> to);
        Task EnviarCorreo(string mensajeHtml, string asunto, string nombreResponsable, string from, string password, List<MailboxAddress> to, List<MailboxAddress> cc, List<MailboxAddress> bcc);
    }
}
