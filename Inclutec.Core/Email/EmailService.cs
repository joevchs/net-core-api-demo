﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;

namespace Inclutec.Core.Email
{
    public class EmailService : IEmailService
    {
        //Clase que contiene los servidores smtp
        private readonly AppConfig _servidoresSmtp;
        private ConfiguracionesGlobales _configGlobales;

        /// <summary>
        /// Constructor del EmailService
        /// </summary>
        /// <param name="servidoresSmtp">Objeto con las configuraciones por servidor de correo</param>
        public EmailService(IOptions<AppConfig> servidoresSmtp, IOptions<ConfiguracionesGlobales> config)
        {
            _servidoresSmtp = servidoresSmtp.Value;
            _configGlobales = config.Value;
        }

        /// <summary>
        /// Envio de correo basico:
        /// Sin copia carbon ni copia oculta ni archivos adjuntos
        /// </summary>
        /// <param name="mensajeHtml">Cuerpo del mensaje en html</param>
        /// <param name="asunto">Asunto del correo</param>
        /// <param name="correoDesde">Direccion de correo desde donde se envia el corre</param>
        /// <param name="contrasenia">Contraseña del correo desde el cual se envia</param>
        /// <param name="destinatarios">Lista de destinatarios para enviar el correo</param>
        /// <returns></returns>
        public async Task EnviarCorreo(string mensajeHtml, string asunto, string correoDesde, string contrasenia, List<MailboxAddress> destinatarios)
        {
            await EnviarCorreo(mensajeHtml, asunto, null, correoDesde, contrasenia, destinatarios, null, null);
        }

        // Enviar correo con las credenciales por default
        public async Task EnviarCorreo(string mensajeHtml, string asunto, List<MailboxAddress> destinatarios)
        {
            var defaultEmail = this._configGlobales.CorreoPrincipal;
            var defaultPassword = this._configGlobales.ContraseniaCorreo;

            await EnviarCorreo(mensajeHtml, asunto, null, defaultEmail, defaultPassword, destinatarios, null, null);
        }

        /// <summary>
        /// Metodo que envia el correo asincronamente 
        /// Recibe todos los parametros
        /// Es importante que los servidores smtp deben estar definidos en el appsettings.json para obtener las configuraciones
        /// </summary>
        /// <param name="mensajeHtml">Cuerpo del mensaje</param>
        /// <param name="asunto">Asunto del correo</param>
        /// <param name="nombreResponsable">Nombre del que envia el correo</param>
        /// <param name="correoDesde">Correo principal desde el cual se envia el correo</param>
        /// <param name="contrasenia">Contraseña para enviar el correo</param>
        /// <param name="destinatarios">Lista de destinatarios</param>
        /// <param name="cc">Lista de copias del correo</param>
        /// <param name="bcc">Lista de copias ocultas del correo</param>
        /// <returns></returns>
        public async Task EnviarCorreo(string mensajeHtml, string asunto, string nombreResponsable, string correoDesde, string contrasenia, List<MailboxAddress> destinatarios, List<MailboxAddress> cc, List<MailboxAddress> bcc)
        {
            //TODO: cambiar a resources en lugar de strings hardcodeados

            // Validar que las configuraciones necesarias para enviar correos no esten vacias
            if (correoDesde == null)
            {
                throw new ArgumentException("La direccion de correo no puede estar vacía.");
            }

            if (contrasenia == null)
            {
                throw new ArgumentException("La contraseña del correo no puede estar vacía.");
            }
            
            var name = _configGlobales.AppName;

            if (_servidoresSmtp.ServidoresSmtp == null)
            {
                throw new ArgumentException("Debe configurar los servidores de correo para enviar correos.");    
            }
            
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(nombreResponsable, correoDesde));
            
            if (destinatarios != null && destinatarios.Any()) {
                // Lista de destinatarios principales
                foreach (var destinatario in destinatarios)
                {
                    emailMessage.To.Add(destinatario);
                }
            }

            if (cc != null && cc.Any()) {
                // Lista de copias
                foreach (var copiaCarbon in cc)
                {
                    emailMessage.Cc.Add(copiaCarbon);
                }
            }

            if (bcc != null && bcc.Any()) {
                // Lista de copias ocultas
                foreach (var copiaOculta in bcc)
                {
                    emailMessage.Bcc.Add(copiaOculta);
                }
            }

            //TODO: probar este codigo para archivos adjuntos
            // create an image attachment for the file located at path
            //var attachment = new MimePart("image", "gif")
            //{
            //    ContentObject = new ContentObject(File.OpenRead(path), ContentEncoding.Default),
            //    ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
            //    ContentTransferEncoding = ContentEncoding.Base64,
            //    FileName = Path.GetFileName(path)
            //};

            //// now create the multipart/mixed container to hold the message text and the
            //// image attachment
            //var multipart = new Multipart("mixed");
            //multipart.Add(body);
            //multipart.Add(attachment);
            //// now set the multipart/mixed as the message body
            //emailMessage.Body = multipart;

            emailMessage.Subject = asunto;

            var bodyBuilder = new BodyBuilder
            {
                HtmlBody = mensajeHtml
            };
            emailMessage.Body = bodyBuilder.ToMessageBody();

            // emailMessage.Body = new TextPart("") { Text = message };

            // Obtener el dominio del correo actual y despues las configuraciones del servidor smtp
            var servidorUsuario = correoDesde.Split('@');
            var servidorSmtp = _servidoresSmtp.ServidoresSmtp.Find(x => x.Servidor.Equals(servidorUsuario[1]));

            
            // Enviar el correo asincronamente
            using (var client = new SmtpClient())
            {
                //await client.ConnectAsync("smtp.live.com", 587, false).ConfigureAwait(false); // SecureSocketOptions.None
                await client.ConnectAsync(
                    servidorSmtp.Dominio,
                    servidorSmtp.Puerto,
                    servidorSmtp.UsarSsl).ConfigureAwait(false);

                client.AuthenticationMechanisms.Remove("XOAUTH2"); // Note: since we don't have an OAuth2 token, disable the XOAUTH2 authentication mechanism.

                // Autenticar al usuario
                await client.AuthenticateAsync(correoDesde, contrasenia).ConfigureAwait(false);
                await client.SendAsync(emailMessage).ConfigureAwait(false);
                await client.DisconnectAsync(true).ConfigureAwait(false);
            }
        }
    }
}
