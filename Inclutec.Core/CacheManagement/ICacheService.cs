﻿using System;

namespace Inclutec.Core.CacheManagement
{
    public interface ICacheService
    {

        void Store(string key, object content);
        void StoreDefaults(string key, object content);
        void Store(string key, object content, int duration);
        void StoreSlidingExpiration(string key, object content, int duration);
        void StoreLowPriority(string key, object content);
        void StoreHighPriority(string key, object content);
        void StoreNeverRemovePriority(string key, object content);
        bool RemoveIfExists(string key);
        T Get<T>(string key) where T : class;
        bool AddItemToCachedList<T>(string key, T newItem) where T : class;
        bool RemoveItemFromCachedList<T>(string key, Predicate<T> predicate) where T : class;
        bool UpdateItemFromCachedList<T>(string key, Predicate<T> predicate, T updatedItem) where T : class;
        T FindItemFromCachedList<T>(string key, Predicate<T> predicate) where T : class;

    }
}