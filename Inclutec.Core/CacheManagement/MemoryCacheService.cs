﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;

namespace Inclutec.Core.CacheManagement
{
    public class MemoryCacheService : ICacheService
    {

        protected readonly IMemoryCache cache;

        /// <summary>Request an instance of IMemoryCache</summary>
        /// <param name="cache">Represents a local in-memory cache whose values are not serialized.</param>
        public MemoryCacheService(IMemoryCache cache)
        {
            this.cache = cache;
        }

        // Defaults
        private static int DefaultDuration => 120;
        private CacheItemPriority defaultPriority => CacheItemPriority.Normal;

        /// <summary>
        ///     Store the value with normal priority and without
        ///     absolute expiration
        /// </summary>
        /// <param name="key">key to use to store the value</param>
        /// <param name="content">Value to be cached</param>
        public void Store(string key, object content)
        {
            StoreNormalPriority(key, content);
        }

        /// <summary>Store the value with default duration and priority</summary>
        /// <param name="key">key to use to store the value</param>
        /// <param name="content">Value to be cached</param>
        public void StoreDefaults(string key, object content)
        {
            Store(key, content, DefaultDuration, defaultPriority);
        }

        /// <summary>Store the value with default priority</summary>
        /// <param name="key">key to use to store the value</param>
        /// <param name="content">Value to be cached</param>
        /// <param name="duration">Expiration duration</param>
        public void Store(string key, object content, int duration)
        {
            Store(key, content, duration, defaultPriority);
        }

        public void StoreHighPriority(string key, object content)
        {
            Store(key, content, DefaultDuration, CacheItemPriority.High);
        }

        public void StoreLowPriority(string key, object content)
        {
            Store(key, content, DefaultDuration, CacheItemPriority.Low);
        }

        public void StoreNeverRemovePriority(string key, object content)
        {
            Store(key, content, DefaultDuration, CacheItemPriority.NeverRemove);
        }

        /// <summary>Removes a cached value if exists</summary>
        /// <param name="key">key to use to look up the value</param>
        public bool RemoveIfExists(string key)
        {
            if (key == null)
            {
                return false;
            }

            object cached;
            if (!cache.TryGetValue(key, out cached))
            {
                return false;
            }

            cache.Remove(key);

            return true;
        }

        /// <summary>Returns the value with the key</summary>
        /// <typeparam name="T">The object type to retrieve</typeparam>
        /// <param name="key">The key to find the value stored</param>
        /// <returns>Returns the value as the type specified; null if not found</returns>
        public T Get<T>(string key) where T : class
        {
            if (key == null)
            {
                return null;
            }

            object result;
            if (cache.TryGetValue(key, out result))
            {
                return result as T;
            }

            return null;
        }

        /// <summary>This method adds a new object to a cached list</summary>
        /// <typeparam name="T">The object type of the list</typeparam>
        /// <param name="key">The key to find the list stored</param>
        /// <param name="newItem">The new object of type T</param>
        /// <returns>Ture if it was succesfully added to the list</returns>
        public bool AddItemToCachedList<T>(string key, T newItem) where T : class
        {
            if (key == null)
            {
                return false;
            }

            object result;
            if (!cache.TryGetValue(key, out result))
            {
                return false;
            }

            var list = result as List<T>;
            if (list != null)
            {
                if (!list.Contains(newItem))
                {
                    list.Add(newItem);
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        /// <summary>This method deletes an existing object of the cached list</summary>
        /// <typeparam name="T">The object type of the list</typeparam>
        /// <param name="key">* The key to find the list stored.</param>
        /// <param name="predicate">* The condition to find the item</param>
        /// <returns>True if it was successfully removed from cache. Otherwise returns false</returns>
        public bool RemoveItemFromCachedList<T>(string key, Predicate<T> predicate) where T : class
        {
            if (key == null)
            {
                return false;
            }

            object result;
            if (!cache.TryGetValue(key, out result))
            {
                return false;
            }

            var list = result as List<T>;
            if (list != null)
            {
                list.RemoveAll(predicate);
            }
            else
            {
                return false;
            }

            return true;
        }

        /// <summary>This method deletes an existing object of the cached list</summary>
        /// <typeparam name="T">The object type of the list</typeparam>
        /// <param name="key">The key to find the list stored</param>
        /// <param name="predicate">* The condition to find the item</param>
        /// <param name="updatedItem">The item updated</param>
        /// <returns>True if it was successfully removed from cache. Otherwise returns false</returns>
        public bool UpdateItemFromCachedList<T>(string key, Predicate<T> predicate, T updatedItem)
            where T : class
        {
            if (key == null)
            {
                return false;
            }

            object result;
            if (!cache.TryGetValue(key, out result))
            {
                return false;
            }

            var list = result as List<T>;
            if (list == null)
            {
                return false;
            }

            // Remove the old value that match the predicate
            list.RemoveAll(predicate);

            // Add the updated value
            list.Add(updatedItem);

            return true;
        }

        /// <summary>
        ///     This generic method finds an existing object from the specified cached list
        ///     The primary key of the object must be called Id
        /// </summary>
        /// <typeparam name="T">The object type of the list</typeparam>
        /// <param name="key">The key to find the list stored</param>
        /// <param name="predicate">* The condition to find the item</param>
        /// <returns>The object that matched the id. Otherwise returs null</returns>
        public T FindItemFromCachedList<T>(string key, Predicate<T> predicate) where T : class
        {
            if (key == null)
            {
                return null;
            }

            object result;
            if (!cache.TryGetValue(key, out result))
            {
                return null;
            }

            var list = result as List<T>;

            return list?.Find(predicate);
        }

        /// <summary>
        ///     Store the value with sliding expiration, can be used
        ///     to keep frequently requested items in the cache
        /// </summary>
        /// <param name="key">key to use to store the value</param>
        /// <param name="content">Value to be cached</param>
        /// <param name="duration">Minutes of sliding expiration</param>
        public void StoreSlidingExpiration(string key, object content, int duration)
        {
            if (key == null)
            {
                return;
            }

            RemoveIfExists(key);

            cache.Set(key, content,
                      new MemoryCacheEntryOptions
                      {
                          SlidingExpiration = TimeSpan.FromMinutes(duration)
                      });
        }

        /// <summary>
        ///     Store the value with default duration and specific priority.
        ///     Use this methods for priorities instead of passing the CacheItemPriority
        ///     to the Store method directly so we don't have to add the the Microsoft.Extensions.Caching.Memory
        ///     dependency.
        /// </summary>
        /// <param name="key">key to use to store the value</param>
        /// <param name="content">Value to be cached</param>
        /// <param name="priority">Priority of the cached value</param>
        public void Store(string key, object content, CacheItemPriority priority)
        {
            Store(key, content, DefaultDuration, priority);
        }

        /// <summary>
        ///     Store the value with specific duration and priority. This method is called from the other methods
        /// </summary>
        /// <param name="key">key to use to store the value</param>
        /// <param name="content">Value to be cached</param>
        /// <param name="duration">Seconds duration</param>
        /// <param name="priority">Priority of the cached value</param>
        public void Store(string key, object content, int duration, CacheItemPriority priority)
        {
            if (key == null)
            {
                return;
            }

            RemoveIfExists(key);

            cache.Set(key, content,
                      new MemoryCacheEntryOptions
                      {
                          AbsoluteExpiration = DateTime.Now + TimeSpan.FromSeconds(duration),
                          Priority = priority
                      });
        }

        /// <summary>
        ///     Store the value with default priority (normal) and without absolute expiration
        /// </summary>
        /// <param name="key">key to use to store the value</param>
        /// <param name="content">Value to be cached</param>
        public void StoreNormalPriority(string key, object content)
        {
            if (key == null)
            {
                return;
            }

            RemoveIfExists(key);

            cache.Set(key, content,
                      new MemoryCacheEntryOptions
                      {
                          Priority = CacheItemPriority.Normal
                      });
        }

    }
}