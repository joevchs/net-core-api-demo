﻿using Inclutec.Core.CacheManagement;
using Inclutec.Core.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;

namespace Inclutec.Core.Persistence
{
    public class GenericService
    {
        protected readonly IMapper mapper;
        protected readonly ICacheService cache;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapperToSet"></param>
        /// <param name="cacheServiceToSet"></param>
        protected GenericService(
            IMapper mapperToSet,
            ICacheService cacheServiceToSet)
        {
            mapper = mapperToSet;
            cache = cacheServiceToSet;
        }

    }

    public class GenericService<TEntity, TEntityDTO> : IService, IGenericService<TEntity, TEntityDTO> 
        where TEntity : class, new()
        where TEntityDTO : class, new()
    {

        protected readonly IMapper mapper;
        protected readonly ICacheService cache;
        private readonly IGenericRepository<TEntity> _repository;
        protected string cachekey;

        protected GenericService(
            IMapper mapperToSet, 
            ICacheService cacheServiceToSet, 
            IGenericRepository<TEntity> repository)
        {
            mapper = mapperToSet;
            cache = cacheServiceToSet;
            _repository = repository;
        }

        /// <summary>
        ///     Search the list of address types from cache or database
        /// </summary>
        /// <returns>List of address types. Otherwise retuns null if not found.</returns>
        public async Task<IEnumerable<TEntityDTO>> GetAllCachedAsync()
        {
            var dtoList = cache.Get<List<TEntityDTO>>(cachekey);

            if (dtoList == null || !dtoList.Any())
            {
                // Replace this for repository and get the list from database async
                var entityList = await _repository.GetAllAsync();

                if (entityList == null)
                {
                    return null;
                }

                // Save as DTO list to cache
                dtoList = mapper.Map<List<TEntityDTO>>(entityList);

                cache.Store(cachekey, dtoList);
            }

            return dtoList;
        }

        public async Task<IEnumerable<TEntityDTO>> GetAllAsync()
        {
            var entityList = await _repository.GetAllAsync();

            if (entityList == null)
            {
                return null;
            }

            // Save as DTO list to cache
            var dtoList = mapper.Map<List<TEntityDTO>>(entityList);
            
            return dtoList;
        }

        /// <summary>
        ///     Get a specific address type from cache or database
        /// </summary>
        /// <param name="id">The item id to get</param>
        /// <returns>Returns the item that match the specified id. Otherwise returns null</returns>
        public async Task<TEntityDTO> GetSingleCachedAsync(int id)
        {
            // First search in cache, if not exist search from database
            var item = cache.FindItemFromCachedList<TEntityDTO>(cachekey, x => GetPropertyValue<int>(x, "Id") == id);

            if (item == null)
            {
                // If not found on cache get from repository and mapp to DTO
                var itemEntity = await _repository.GetSingleAsync(x => GetPropertyValue<int>(x, "Id") == id);
                item = mapper.Map<TEntityDTO>(itemEntity);
            }

            return item;
        }

        public async Task<TEntityDTO> GetSingleAsync(int id)
        {
            // If not found on cache get from repository and mapp to DTO
            var itemEntity = await _repository.GetSingleAsync(x => GetPropertyValue<int>(x, "Id") == id);
            var item = mapper.Map<TEntityDTO>(itemEntity);
            
            return item;
        }

        public async Task<IEnumerable<TEntityDTO>> GetAllAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var entityList = await _repository.GetAllAsync(predicate);

            if (entityList == null)
            {
                return null;
            }

            // Save as DTO list to cache
            var dtoList = mapper.Map<List<TEntityDTO>>(entityList);

            return dtoList;
        }

        public async Task<PagedResult<TEntity>> GetByPage(int page, int size)
        {
            /*var dtoList = _cache.Get<PagedResult<TEntity>>(_llaveCache);

            if (dtoList == null || !dtoList.Results.Any())
            {*/
            var entityList = await _repository.GetPaged(page, size);

            //var dtoList = mapper.Map<List<TEntityDTO>>(entityList.Results);

            //var t = new PagedResult();
            /*_cache.Store(_llaveCache, listaDto);
        }
        */
            return entityList;
        }

        /// <summary>
        ///     Adds a new item to the database and cache
        /// </summary>
        /// <param name="itemDto">The new object to add</param>
        public int? Add(TEntityDTO itemDto)
        {
            var exists = _repository.Exists(x => GetPropertyValue<string>(x, "Name") == GetPropertyValue<string>(itemDto, "Name"));

            if (exists)
            {
                return null;
            }
            var itemEntity = mapper.Map<TEntity>(itemDto);

            // Add the item to database and get the autogenerated id
            _repository.Add(itemEntity);
            _repository.Commit();

            var item = mapper.Map<TEntityDTO>(itemEntity);
            // Adds the new addressType to the list if its cached
            cache.AddItemToCachedList(cachekey, item);

            var id = GetPropertyValue<int>(item, "Id");
            return id;
        }

        /// <summary>
        ///     Updates an existing item on database and cache
        /// </summary>
        /// <param name="updatedItem">The updated item</param>
        /// <param name="id">The id of the item to updated</param>
        /// <returns>Returns true id the item was found and updated. Otherwise returns false.</returns>
        public async Task<bool> Update(TEntityDTO updatedItem, int id)
        {
            // Search the item on the database
            var item = await _repository.GetSingleAsync(x => GetPropertyValue<int>(x, "Id") == id);

            if (item == null)
            {
                return false;
            }

            //Map DTO to model - the entity id gets overwritten
            item = mapper.Map(updatedItem, item);
            SetValue(item, "Id", id);

            // Edit the row on the DB
            _repository.Update(item);
            _repository.Commit();

            // Find the cached item by id from the cached list that is the same type as the addressType item
            cache.UpdateItemFromCachedList(cachekey, x => GetPropertyValue<int>(x, "Id") == id, updatedItem);

            return true;
        }

        /// <summary>
        ///     Deletes an existing item
        /// </summary>
        /// <param name="id">The id of the item to delete</param>
        /// <returns>Returns true if the item was found and deleted. Otherwise returns false</returns>
        public bool Delete(int id)
        {
            // Search the item to delete
            var item = _repository.GetSingle(x => GetPropertyValue<int>(x, "Id") == id);

            if (item == null)
            {
                return false;
            }

            // Here remove from database
            _repository.Delete(item);
            _repository.Commit();

            // Remove from the cached list
            cache.RemoveItemFromCachedList<TEntityDTO>(cachekey, x => GetPropertyValue<int>(x, "Id") == id);

            return true;
        }

        /// <summary>Returns the value of an object property using reflection</summary>
        /// <typeparam name="T">The value type</typeparam>
        /// <param name="column">The object column to filter</param>
        /// <param name="propertyName">The name of the property</param>
        /// <returns>The value cast to the given parameter</returns>
        public static T GetPropertyValue<T>(object column, string propertyName)
        {
            return (T)column.GetType().GetTypeInfo().GetProperty(propertyName).GetValue(column, null);
        }

        /// <summary>Set a property value by using reflection</summary>
        /// <returns>The entity</returns>
        public object SetValue<T>(object entity, string prop, T value)
        {
            var type = entity.GetType();
            var property = type.GetProperty(prop);
            property.SetValue(entity, value);

            return entity;
        }
    }
}