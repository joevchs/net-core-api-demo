﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Inclutec.Core.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Inclutec.Core.Persistence
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class, new()
    {
        public readonly DbContext _context;
        private readonly ILogger _logger;
        public DbSet<TEntity> ThisContext => _context.Set<TEntity>();

        public GenericRepository(
            DbContext context,
            ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger($"GenericRepository<{typeof(TEntity)}>");
        }

        /// <summary>
        ///     Selecciona todos los registros
        /// </summary>
        public virtual IEnumerable<TEntity> GetAll()
        {
            IEnumerable<TEntity> entityList = null;
            entityList = ThisContext.ToList(); 

            return entityList;
        }

        /// <summary>
        ///     Selecciona todos los registros asincronamente
        /// </summary>
        public virtual async Task<List<TEntity>> GetAllAsync()
        {
            List<TEntity> entityList = null;

            entityList = await ThisContext.ToListAsync();
            
            return entityList;
        }

        /// <summary>
        ///    Selecciona la cantidad de registros
        /// </summary>
        public virtual int Count()
        {
            var count = 0;
            count = ThisContext.Count();

            return count;
        }

        /// <summary>
        ///     Selecciona la cantidad de registros asincronamente
        /// </summary>
        public virtual async Task<int> CountAsync()
        {
            var count = 0;
            count = await ThisContext.CountAsync();

            return count;
        }

        /// <summary>Determines whether any element of a sequence satisfies a condition</summary>
        /// <param name="predicate">The expression to validate</param>
        /// <returns>True if the condition is satisfied, otherwise false</returns>
        public virtual bool Exists(Expression<Func<TEntity, bool>> predicate)
        {
            var exists = false;
            exists = ThisContext.Any(predicate);

            return exists;
        }

        /// <summary>Asynchronously determines whether any element of a sequence satisfies a condition</summary>
        /// <param name="predicate">The expression to validate</param>
        /// <returns>True if the condition is satisfied, otherwise false</returns>
        public virtual async Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var exists = false;
            exists = await ThisContext.AnyAsync(predicate);

            return exists;
        }

        /// <summary>Specify related entities to include in the results</summary>
        /// <param name="includeProperties">Expression with all the properties</param>
        /// <returns>Creates a Enumerable T from an IQuerable</returns>
        public virtual IEnumerable<TEntity> AllIncluding(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IEnumerable<TEntity> entityList = null;
            IQueryable<TEntity> query = ThisContext;

            query = includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
            entityList = query.ToList();

            return entityList;
        }

        /// <summary>Specify related entities to include in the results</summary>
        /// <param name="includeProperties">Expression with all the properties</param>
        /// <returns>Asynchronously creates a List T from an IQuerable by enumerating it asynchronously</returns>
        public virtual async Task<IEnumerable<TEntity>> AllIncludingAsync(
            params Expression<Func<TEntity, object>>[] includeProperties) {
            IEnumerable<TEntity> entityList = null;
            IQueryable<TEntity> query = ThisContext;

            query = includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            entityList = await query.ToListAsync();

            return entityList;
        }

        /// <summary>
        ///     Returns the first element of a sequence that satisfies a specified
        ///     condition or a default value if not such element is found
        /// </summary>
        /// <param name="predicate">The expression to validate</param>
        /// <returns>The first element or null if not found</returns>
        public TEntity GetSingle(Expression<Func<TEntity, bool>> predicate)
        {
            TEntity entity = null;

            entity = ThisContext.FirstOrDefault(predicate);

            return entity;
        }

        /// <summary>
        ///     Asynchronously returns the first element of a sequence that satisfies a specified
        ///     condition or a default value if not such element is found
        /// </summary>
        /// <param name="predicate">The expression to validate</param>
        /// <returns>The first element or null if not found</returns>
        public async Task<TEntity> GetSingleAsync(Expression<Func<TEntity, bool>> predicate)
        {
            TEntity entity = null;

            entity = await ThisContext.FirstOrDefaultAsync(predicate);

            return entity;
        }

        /// <summary>
        ///     Returns the first element of a sequence that satisfies a specified
        ///     condition or a default value if not such element is found
        /// </summary>
        /// <param name="predicate">The expression to validate</param>
        /// <param name="includeProperties"></param>
        /// <returns>The first element or null if not found</returns>
        public TEntity GetSingle(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            TEntity entity = null;
            IQueryable<TEntity> query = ThisContext;
            query = includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            entity = query.Where(predicate).FirstOrDefault();

            return entity;
        }

        /// <summary>
        ///     Asynchronously returns the first element of a sequence that satisfies a specified
        ///     condition or a default value if not such element is found
        /// </summary>
        /// <param name="predicate">The expression to validate</param>
        /// <param name="includeProperties"></param>
        /// <returns>The first element or null if not found</returns>
        public async Task<TEntity> GetSingleAsync(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            TEntity entity = null;
            IQueryable<TEntity> query = ThisContext;
            query = includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            entity = await query.Where(predicate).FirstOrDefaultAsync();

            return entity;
        }

        public async Task<List<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate)
        {
            List<TEntity> entity = null;

            entity = await ThisContext.Where(predicate).ToListAsync();

            return entity;
        }

        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate)
        {
            IEnumerable<TEntity> entity = null;

            entity = ThisContext.Where(predicate).ToList();

            return entity;
        }

        /// <summary>Begins tracking the given entity</summary>
        /// <param name="entity">The entity to save</param>
        public virtual void Add(TEntity entity)
        {
            ThisContext.Add(entity);
        }

        /// <summary>Begins tracking the given entities</summary>
        /// <param name="entity">The entities to save</param>
        public virtual void Add(IEnumerable<TEntity> entity)
        {
            ThisContext.AddRange(entity);
        }

        /// <summary>The entity will be marked as modified</summary>
        /// <param name="entity">The entity to modify</param>
        public virtual void Update(TEntity entity)
        {
            var dbEntityEntry = _context.Entry(entity);
            dbEntityEntry.State = EntityState.Modified;
        }

        /// <summary>The entity will be marked for deletion regardless of its current state</summary>
        /// <param name="entity">The entity to delete</param>
        public virtual void Delete(TEntity entity)
        {
            var dbEntityEntry = _context.Entry(entity);
            dbEntityEntry.State = EntityState.Deleted;
        }

        /// <summary>The entities will be marked for deletion regardless of its current state</summary>
        /// <param name="predicate">The condition to evaluate</param>
        public virtual void DeleteWhere(Expression<Func<TEntity, bool>> predicate)
        {
            IEnumerable<TEntity> entities = ThisContext.Where(predicate);

            foreach (var entity in entities)
            {
                _context.Entry(entity).State = EntityState.Deleted;
            }
        }

        /// <summary>Saves all the changes made in this context to the database.
        /// This is called directly when it is not neccesary the use of UnitOfWork</summary>
        public virtual void Commit()
        {
            _context.SaveChanges();
        }

        public virtual async Task CommitAsync()
        {
           await _context.SaveChangesAsync();
        }

        // Metodo generico para paginación en el servidor
        public async Task<PagedResult<TEntity>> GetPaged(int page, int pageSize)
        {
            IQueryable<TEntity> query = ThisContext;

            var result = new PagedResult<TEntity>
            {
                CurrentPage = page,
                PageSize = pageSize,
                RowCount = query.Count()
            };

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page - 1) * pageSize;
            result.Results = await query.Skip(skip).Take(pageSize).AsNoTracking().ToListAsync();

            return result;
        }
    }
}