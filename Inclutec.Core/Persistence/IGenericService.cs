﻿using Inclutec.Core.Generic;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Inclutec.Core.Persistence
{
    public interface IGenericService<TEntity, TEntityDTO> : IService
        where TEntity : class, new()
        where TEntityDTO : class, new()
    {

        Task<IEnumerable<TEntityDTO>> GetAllAsync(Expression<Func<TEntity, bool>> predicate);
        Task<IEnumerable<TEntityDTO>> GetAllAsync();
        Task<TEntityDTO> GetSingleAsync(int id);

        Task<IEnumerable<TEntityDTO>> GetAllCachedAsync();        
        Task<TEntityDTO> GetSingleCachedAsync(int id);
        Task<PagedResult<TEntity>> GetByPage(int page, int size);
        int? Add(TEntityDTO entity);        
        Task<bool> Update(TEntityDTO entity, int id);
        bool Delete(int id);
       
    }
}