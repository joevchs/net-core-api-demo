﻿using System;
using System.Collections.Generic;

namespace Inclutec.Core.Generic
{
    public abstract class PagedResultBase
    {

        public int CurrentPage { get; set; }
        public int PageCount { get; set; }
        public int PageSize { get; set; }
        public int RowCount { get; set; }

        public int FirstRowOnPage => (CurrentPage - 1) * PageSize + 1;

        public int LastRowOnPage => Math.Min(CurrentPage * PageSize, RowCount);

    }

    public class PagedResult<TEntity> : PagedResultBase
    {

        public List<TEntity> Results { get; set; }

        public PagedResult()
        {
            Results = new List<TEntity>();
        }

        public bool HasPreviousPage => (CurrentPage > 1);

        public bool HasNextPage => (CurrentPage < PageCount);

    }
}
