﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using Inclutec.Core.Email;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using MimeKit;
using Newtonsoft.Json;

namespace Inclutec.Core.Filters
{
    public class GlobalExceptionFilter : IExceptionFilter
    {
        
        private readonly ILogger _logger;
        private IEmailService _emailService;

        public GlobalExceptionFilter(ILoggerFactory loggerFactory, IEmailService emailService)
        {
            this._emailService = emailService;
            _logger = loggerFactory.CreateLogger("GlobalExceptionFilter");
        }

        /// <summary>
        /// What happens on any exception.
        /// This method can't be asyncronous since it has to return a response to the client.
        /// </summary>
        public void OnException(ExceptionContext context)
        {
            // Exception details - default Internal server error 500
            var exception = context.Exception;
            var response = context.HttpContext.Response;
            var status = HttpStatusCode.InternalServerError;
            var errorMessage = exception.Message;
            var innerException = exception.InnerException;
            var source = exception.Source;
            var exceptionType = exception.GetType();
            var stack = exception.StackTrace;

            // Project Source, error file location
            var controller = context.RouteData.Values["controller"];
            var actionName = context.RouteData.Values["action"];
            var lineNumber = 0;
            var colNumber = 0;
            var fileName = "";
            var trace = new StackTrace(exception, true);
            var frame = trace.GetFrames().FirstOrDefault();

            if (frame != null)
            {
                // This is possible thanks to "debugType": "portable"
                lineNumber = frame.GetFileLineNumber();
                colNumber = frame.GetFileColumnNumber();
                fileName = frame.GetFileName();
            }

            // Request Connection IP
            var ipv4Address = string.Empty;
            var connection = context.HttpContext.Connection;
            var ip = connection?.RemoteIpAddress;

            if (ip != null)
            {
                var ipv4 = ip.MapToIPv4();
                ipv4Address = ipv4.ToString();
            }

            // Current user identity
            var principal = context.HttpContext.User;
            var identity = principal.Identity as ClaimsIdentity;
            var fullname = "N/A";

            if (identity != null)
            {
                if (identity.IsAuthenticated)
                {
                    try
                    {
                        fullname = identity.FindFirst("FullName").Value;
                    }
                    catch (Exception e)
                    {
                        _logger.LogWarning(default(EventId), "User Full Name wasn't found in the Bearer Token Claims. Error: "+e.Message);
                    }
                }
            }

            // Gets the default custom exception message defined on the resources.
            var isCritical = false;
            var customMessage = "Error interno del servidor";

            // Common exceptions, overrides the defaults with multilenguage
            if (exception is UnauthorizedAccessException)
            {
                customMessage = "No autorizado";
                
                status = HttpStatusCode.Unauthorized;
            }
            else if (exception is NotImplementedException)
            {
                customMessage = "El recurso accedido esta en desarrollo";
                status = HttpStatusCode.NotImplemented;
            }
            else if (exception is ArgumentNullException)
            {
                customMessage = "Error de null";
            }
            else if (exception is DivideByZeroException)
            {
                customMessage = "Error division por 0";
            }
            else if (exception is FileNotFoundException)
            {
                customMessage = "Error no se encontro el archivo";
                status = HttpStatusCode.NotFound;
            } else
            {
                customMessage = "Error interno del servidor";
            }

            response.StatusCode = (int)status;
            response.ContentType = "application/json";
            
            //Loggea el stacktrace (formato json) dentro del campo "Message" del log.
            var logStackTrace = JsonConvert.SerializeObject(new
                                                               {
                                                                   Error = new
                                                                   {
                                                                       UserName = fullname,
                                                                       FileName = fileName,
                                                                       Controller = controller,
                                                                       ActionMethod = actionName,
                                                                       LineNumber = lineNumber,
                                                                       ColumnLineNnumber = colNumber,
                                                                       StatusCode = (int)status,
                                                                       CustomMessage = customMessage,
                                                                       ErrorMessage = errorMessage,
                                                                       StackTrace = stack,
                                                                       Source = source,
                                                                       InnerException = innerException,
                                                                       Exception = exceptionType.Name,
                                                                       IP = ipv4Address
                                                                   }
                                                               });

            //Log to database every exception async:
            if (isCritical)
            {
                _logger.LogCritical(logStackTrace);
            }
            else
            {
                _logger.LogError(logStackTrace);
            }

            var to = new List<MailboxAddress>
            {
                new MailboxAddress("jostin95ev@hotmail.com")
            };

            // _emailService.EnviarCorreo("Ocurrió un error en el sistema", "ALERTA DE ERROR", "evandbmw@gmail.com", "123123", to);
            
            //Es necesario que sea especificado en "context.Result" para retornar la respuesta al cliente
            context.Result = new ObjectResult(customMessage)
            {
                StatusCode = (int)status,
                DeclaredType = exception.GetType()
            };
        }

    }
}