﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Demo.Entities.Models;
using Inclutec.Core.Persistence;

namespace Demo.Repositories.Interfaces
{
    public interface IPostsRepository: IGenericRepository<Post>
    {
        Task<IEnumerable<Post>> getAllAsync();
    }
}
