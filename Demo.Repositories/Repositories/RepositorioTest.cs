﻿using System;
using System.Collections.Generic;
using System.Text;
using Demo.Entities.Models;
using Demo.Repositories.Interfaces;
using Inclutec.Core.Persistence;
using Microsoft.Extensions.Logging;

namespace Demo.Repositories.Repositories
{
    public class RepositorioTest : GenericRepository<Salaries>, IRepositorioTest
    {
        public RepositorioTest(employeesContext context, ILoggerFactory loggerFactory) : base(context, loggerFactory)
        {
        }
    }
}
