﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Demo.Entities.Models;
using Demo.Repositories.Interfaces;
using Inclutec.Core.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Demo.Repositories.Repositories
{
    public class PostsRepository : GenericRepository<Post>, IPostsRepository
    {
        public PostsRepository(employeesContext context, ILoggerFactory loggerFactory) : base(context, loggerFactory)
        {
        }

        public async Task<IEnumerable<Post>> getAllAsync()
        {
            return await this.ThisContext
                .Include(x=>x.Blog)
                .Include(x=>x.Author)
                .AsNoTracking()
                .ToListAsync();
        }
    }
}
