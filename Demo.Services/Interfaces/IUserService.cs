﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Demo.Entities.DTO;
using Inclutec.Core.Generic;

namespace Demo.Services.Interfaces
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        IEnumerable<User> GetAll();
    }
}
