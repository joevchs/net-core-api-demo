﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Demo.Entities.Models;
using Inclutec.Core.Generic;

namespace Demo.Services.Interfaces
{
    public interface IServicioTest
    {
        Task<PagedResult<Salaries>> getAllSalariesCachedAsync();
        Task<PagedResult<Salaries>> getAllSalariesAsync();

        Task<IEnumerable<Post>> getIncludedEntitiesAsync();
    }
}
