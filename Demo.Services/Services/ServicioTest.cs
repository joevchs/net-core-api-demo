﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Demo.Services.Interfaces;
using Demo.Entities;
using Demo.Entities.Models;
using Demo.Repositories.Interfaces;
using System.Threading.Tasks;
using Inclutec.Core.Generic;
using Inclutec.Core.CacheManagement;

namespace Demo.Services.Services
{
    public class ServicioTest: IServicioTest
    {
        #region Declaracion de variables 
        private IRepositorioTest _repositorioTest;
        private IPostsRepository _postsRepository;
        protected readonly ICacheService cache;
        protected string cachekey = ECacheKeys.Test.ToString();
        #endregion

        /// <summary>
        /// Constructor Dependency injection del repositorio y del servicio de cache.
        /// </summary>
        public ServicioTest(IRepositorioTest repositorioTest, IPostsRepository postsRepository, ICacheService cacheServiceToSet) {
            this._repositorioTest = repositorioTest;
            this._postsRepository = postsRepository;
            cache = cacheServiceToSet;
        }

        /// <summary>
        /// Si la entrada no existe en cache se busca directamente en base de datos.
        /// </summary>
        public async Task<PagedResult<Salaries>> getAllSalariesCachedAsync() {
            var dtoList = cache.Get<PagedResult<Salaries>>(cachekey);

            if (dtoList == null || dtoList.RowCount == 0)
            {
                // get the list from database async
                dtoList = await _repositorioTest.GetPaged(1, 1000);

                if (dtoList == null)
                {
                    return null;
                }

                // store in cache
                cache.Store(cachekey, dtoList);
            }

            return dtoList;
        }

        public async Task<PagedResult<Salaries>> getAllSalariesAsync() {
            var dtoList = await _repositorioTest.GetPaged(1, 1000);

            return dtoList;
        }

        public async Task<IEnumerable<Post>> getIncludedEntitiesAsync()
        {
            var results1 = await _postsRepository.getAllAsync();
            // var results2 = await _postsRepository.AllIncludingAsync(x => x.Salaries);

            return results1;
        }
    }
}
