﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Demo.Services.Interfaces;
using Demo.Entities;
using Demo.Entities.DTO;
using Demo.Repositories.Interfaces;
using System.Threading.Tasks;
using Inclutec.Core.Generic;
using Inclutec.Core.CacheManagement;
using Inclutec.Core.Email;
using Microsoft.Extensions.Options;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;

namespace Demo.Services.Services
{
    public class UserService: IUserService
    {
        // Lista de usuarios hardcoded
        private List<User> _users = new List<User>
        { 
            new User { Id = 1, Nombre = "Jostin", Apellidos = "Chaves", NombreUsuario = "joevchs", Contrasenia = "123", Rol = "ADMIN" },
            new User { Id = 2, Nombre = "Harry", Apellidos = "Muir", NombreUsuario = "hmuir", Contrasenia = "123", Rol = "USER" },
            new User { Id = 3, Nombre = "Victor", Apellidos = "Romero", NombreUsuario = "vromero", Contrasenia = "123", Rol = "QA" },
            new User { Id = 1, Nombre = "Mike", Apellidos = "Sanchez", NombreUsuario = "msanchez", Contrasenia = "123", Rol = "QA" },
            new User { Id = 1, Nombre = "Test", Apellidos = "Test Apellidos", NombreUsuario = "test", Contrasenia = "test", Rol = "MAINTAINER" } 
        };

        private readonly ConfiguracionesGlobales _appSettings;

        /// <summary>
        /// Constructor con dependency injection.
        /// </summary>
        public UserService(IOptions<ConfiguracionesGlobales> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public User Authenticate(string username, string password)
        {
            var user = _users.SingleOrDefault(x => x.NombreUsuario == username && x.Contrasenia == password);

            // No se encontro el usuario
            if (user == null)
                return null;

            // La autenticacion fue exitosa generar el token jwt
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim(ClaimTypes.Name, user.Nombre.ToString()),
                    new Claim(ClaimTypes.Role, user.Rol.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // Limpiar contraseña
            user.Contrasenia = null;

            return user;
        }

        public IEnumerable<User> GetAll()
        {
            // Usuarios sin contraseña
            return _users.Select(x => {
                x.Contrasenia = null;
                return x;
            });
        }
    }
}
