﻿using AutoMapper;

namespace Demo.Entities
{
    public class AutoMapperProfileConfiguration : Profile
    {

        public AutoMapperProfileConfiguration()
        {

            // ReverseMap() permite mapear en ambos sentidos (de las clases de EF a DTO y de DTO a clases de EF)
            // CreateMap<Categories, CategoriesDTO>().ReverseMap();
            

        }
    }
}
