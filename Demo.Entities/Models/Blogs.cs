﻿using System;
using System.Collections.Generic;

namespace Demo.Entities.Models
{
    public partial class Blogs
    {
        public Blogs()
        {
            Post = new HashSet<Post>();
        }

        public int Id { get; set; }
        public string Descripcion { get; set; }

        public ICollection<Post> Post { get; set; }
    }
}
