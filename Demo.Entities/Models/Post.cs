﻿using System;
using System.Collections.Generic;

namespace Demo.Entities.Models
{
    public partial class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int BlogId { get; set; }
        public int? AuthorId { get; set; }

        public Authors Author { get; set; }
        public Blogs Blog { get; set; }
    }
}
