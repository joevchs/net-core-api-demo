﻿using System;
using System.Collections.Generic;

namespace Demo.Entities.Models
{
    public partial class DeptManager
    {
        public int EmpNo { get; set; }
        public string DeptNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public Departments DeptNoNavigation { get; set; }
        public Employees EmpNoNavigation { get; set; }
    }
}
