﻿using System;
using System.Collections.Generic;

namespace Demo.Entities.Models
{
    public partial class Authors
    {
        public Authors()
        {
            Post = new HashSet<Post>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Post> Post { get; set; }
    }
}
