namespace Demo.Entities.DTO
{
    public class User
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string NombreUsuario { get; set; }
        public string Contrasenia { get; set; }
        public string Token { get; set; }
        public string Rol { get; set; }
    }
}