using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Demo.Entities.DTO
{
    public partial class Item
    {
        [Required]
        public int Id { get; set; }
        
        [Required]
        [StringLength(10)]
        public string Nombre { get; set; }
        
        public string Telefono { get; set; }

        [Range(0, 999.99)]
        public decimal Precio { get; set; }

        [RegularExpression("[a-z]")]
        public string SoloLetras { get; set; }
    }
}
