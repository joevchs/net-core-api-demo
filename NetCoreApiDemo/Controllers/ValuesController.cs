﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.Entities.Models;
using Demo.Services.Interfaces;
using Inclutec.Core.Email;
using Inclutec.Core.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace NetCoreApiDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private IServicioTest _servicioTest;

        public ValuesController(IServicioTest servicioTest) {
            this._servicioTest = servicioTest;
        }

        [HttpGet("cacheResults")]
        public async Task<ActionResult> GetCachedAsync()
        {
            var lista = await this._servicioTest.getAllSalariesCachedAsync();

            return Ok(lista);
        }

        [HttpGet]
        public async Task<ActionResult> GetAsync()
        {
            var lista = await this._servicioTest.getAllSalariesAsync();

            return Ok(lista);
        }

        [HttpGet("SimulateException")]
        public ActionResult SimulateException()
        {
            throw new Exception("Error interno simulado");

            return Ok();
        }

        [HttpGet("relatedEntities")]
        public async Task<ActionResult> GetRelatedEntitiesAsync()
        {
            var lista = await this._servicioTest.getIncludedEntitiesAsync();

            return Ok(lista);
        }
        
        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
