﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.Entities.DTO;
using Demo.Entities.Models;
using Demo.Services.Interfaces;
using Inclutec.Core.Email;
using Inclutec.Core.Filters;
using Inclutec.Core.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace NetCoreApiDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OtroController : ControllerBase
    {

        public OtroController() {
        }

        /// <summary>
        /// Este metodo solo puede ser accedido por el role de ADMIN.
        /// </summary>
        [Authorize(Roles = "ADMIN")]
        [HttpGet]
        public ActionResult<string> Get()
        {
            return "ERES UN ADMIN AUTORIZADO!!";
        }

        [HttpGet("queryParams")]
        public ActionResult Get(int id, string qparam2, string qparam3)
        {

            return Ok();
        }

        /// <summary>
        /// Obtiene un elemento según el id especificado.
        /// </summary>
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        /// <summary>
        /// Crea un nuevo elemento.
        /// </summary>
        /// <remarks>
        /// Ejemplo json creación:
        ///
        ///     POST /Item
        ///     {
        ///        "id": 1,
        ///        "nombre": "Item1",
        ///        "telefono": "123123"
        ///     }
        ///
        /// </remarks>
        [HttpPost]
        [ValidateModel]
        public IActionResult Post([FromBody] Item value)
        {   
            return Ok("Se creó correctamente el item");
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
