using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

public class MiddlewareEjemplo3
{
    private readonly RequestDelegate _next;
    private readonly ILogger<MiddlewareEjemplo1> _logger;

    // Constructor, injectar servicios y configuracion
    public MiddlewareEjemplo3 (RequestDelegate next, ILoggerFactory loggerFactory)
    {
        _next = next ?? throw new ArgumentNullException(nameof(next));
        _logger = loggerFactory?.CreateLogger<MiddlewareEjemplo1>() ?? throw new ArgumentNullException(nameof(loggerFactory));
    }

    public async Task Invoke(HttpContext context)
    {
        // *** Aqui puedes ejecutar cierta lógica antes de pasar al siguiente middleware ***
        _logger.LogDebug("Ejecutando middleware 3");
        
        await _next.Invoke(context);

        // *** Aqui puedes ejecutar cierta lógica despues de haber retornado la respuesta ***
        _logger.LogDebug("Despues de ejecutar middleware 3");

        return;
    }
}