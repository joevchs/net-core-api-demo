﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inclutec.Core.CacheManagement;
using Inclutec.Core.Email;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AutoMapper;
using Demo.Entities;
using Demo.Services;
using Demo.Entities.Models;
using Demo.Services.Interfaces;
using Demo.Services.Services;
using Microsoft.EntityFrameworkCore;
using Demo.Repositories.Repositories;
using Demo.Repositories.Interfaces;
using Inclutec.Core.Filters;   
using Microsoft.Extensions.Configuration.Binder;
using Swashbuckle.AspNetCore.Swagger;
using System.Reflection;
using System.IO;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Newtonsoft.Json;

namespace NetCoreApiDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // En este metodo se agrega la configuración de todos los servicios que vamos a utilizar.
        public void ConfigureServices(IServiceCollection services)
        {
            // Configurar conexion a provider de bd
            var connection = Configuration.GetConnectionString("MySqlConnectionString");
            services.AddDbContext<employeesContext>(options => options.UseMySql(connection));
            // Comando para generar modelos: dotnet ef dbcontext scaffold "server=localhost;port=3306;user=root;password=130023;database=employees" "Pomelo.EntityFrameworkCore.Mysql" -o Models

            // Nos permite tener la configuracion strongly typed en clases/POCOs
            services.AddOptions();
            services.Configure<ConfiguracionesGlobales>(Configuration.GetSection("ConfiguracionesGlobales"));
            services.Configure<AppConfig>(Configuration.GetSection("AppConfig"));

            // Registrar servicios y repositorios segun el tiempo de vida
            services.AddSingleton<ICacheService, MemoryCacheService>();
            services.AddTransient<IEmailService, EmailService>();
            services.AddScoped<IServicioTest, ServicioTest>();
            services.AddScoped<IRepositorioTest, RepositorioTest>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPostsRepository, PostsRepository>();

            // Agregar servicio de cache
            services.AddMemoryCache();

            // Autenticación
            var key = Encoding.ASCII.GetBytes(Configuration["ConfiguracionesGlobales:Secret"]);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                    // más opciones de configuración
                };
            });

            // Podemos configurar para ingresar con un proveedor externo como:
            /*services.AddAuthentication()
                    .AddMicrosoftAccount(microsoftOptions => { ... })
                    .AddGoogle(googleOptions => { ... })
                    .AddTwitter(twitterOptions => { ... })
                    .AddFacebook(facebookOptions => { ... });*/
            
            // Agregar automapper
            services.AddAutoMapper();
            var mapperconfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfileConfiguration());
            });
            services.AddSingleton(sp => mapperconfig.CreateMapper());

            // services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            
            services.AddMvc()
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
            .AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;
                options.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
            });

            // Agregar libreria para documentación del api
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info 
                { 
                    Title = "Net Core API Demo", 
                    Version = "v1" ,
                    Description = "Un ejemplo de documentación en Net Core con swagger",
                    Contact = new Contact { Name = "Inclutec", Email = string.Empty, Url = "https://inclutecr.com"},
                    License = new License { Name = "Use under ###", Url = "https://example.com/license" }
                });

                c.SwaggerDoc("v2", new Info
                {
                    Title = "Net Core API Demo",
                    Version = "v2",
                    Description = "Podemos tener más de una versión de nuestro api",
                    Contact = new Contact() { Name = "Inclutec", Email = string.Empty, Url = "https://inclutecr.com" }
                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // En este metodo se configuran los requests del pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                // Mostrar pantalla detallada del error en html si estamos en el navegador
                app.UseDeveloperExceptionPage();

                // Enables the app to use Swagger with the configuration in the ConfigureServices method.
                app.UseSwagger();
                
                // Enables Swagger UI / view our API calls.
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Swagger XML Api Demo v1");
                    c.SwaggerEndpoint("/swagger/v2/swagger.json", "Swagger XML Api Demo v2");
                });
            }
            else if (env.IsStaging())
            {
                // Agregar al pipeline de pruebas
                // ...
            } 
            else if (env.IsProduction())
            {
                // Agregar al pipeline de producción
                // ...
            }
            else 
            {
                app.UseHsts(); // Strict transport security
            }

            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

            app.UseAuthentication();
            
            // app.UseHttpsRedirection();

            // 1ro en el pipeline
            app.UseMiddleware<ExceptionHandlingMiddleware>();
            app.UseMiddleware<MiddlewareEjemplo1>();
            app.UseMiddleware<MiddlewareEjemplo2>();
            app.UseMiddleware<MiddlewareEjemplo3>();

            app.UseMvc();
        }
    }
}
